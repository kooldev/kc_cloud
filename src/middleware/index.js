import { Router } from 'express';
import bcrypt from 'bcryptjs';
import loki from 'lokijs'
import jwt from 'jsonwebtoken';
import url from 'url'

export default ({config, db}) => {	
	const urlParams = (req) => {
		if (req.method==='POST') {
			return req.fields;
		} else if (req.method==='GET') {
			return req.query;
		} else {
			throw new Error('bad url method');
		}
	}

	const getUser = (user) => {
		const userCollection = db.getCollection('users').find({'user': user });
		if (!userCollection || userCollection.length===0 ) {				
			throw new Error('no user');
		}
		return userCollection[0];
	}

	const signIn = (req) => {
		let user = urlParams(req).user;
		let password = urlParams(req).password;

		try {
			const userCollection = db.getCollection('users').find({'user': user });
			if (!userCollection || userCollection.length===0 ) {				
				throw new Error('no user');
			}
			const userJSON = getUser(user);
			if (!password) {
				return { session:false};
			} else {
				if (bcrypt.compareSync(password, userJSON.password)) {
					let token = jwt.sign({
						data: userJSON.password,
						iat: 1400062400223,
					}, 'secret2');

					return { session:true, token: token};
				} else {
					return { session:false };
				}
			}
		} catch (e) {
			return { session:false};
		}
	}

	const isSignedIn = (req) => {
		try {
			var decoded = jwt.verify(urlParams(req).token, 'secret2');
			
			const userJSON = getUser(urlParams(req).user);
			if(decoded.data !== userJSON.password) {
				return false
			}
			return true;
		} catch (e) {
			return false;
		}
	}

	return (function (req, res, next) {
		const urlPathName = url.parse(req.url, true, true).pathname;
		
		if (urlPathName==='/' || urlPathName==='/takes') {
			return next('route');
		}

		if (urlPathName==='/signin') {
			return res.send(signIn(req));
		}
		const getquery =url.parse(req.url, true, true).query;

		if (!req.fields.path && !getquery.path) {
			return next('unexpectedfields');
		}

		const signedIn = isSignedIn(req, db);
		if (signedIn) {
			return next('route');
		} else {
			return next('nologin');
		}
	});
}
