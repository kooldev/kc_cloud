export default () => {	
	return function (err, req, res, next) {
    if (err === 'nologin') {
      res.send({ session:false })
      res.end();
      return next('nologin'); 
    } else if (err === 'unexpectedfields') {
      res.send({ session: true, error: 'NO EXPECTED FIELDS FOUND' });
      return next('unexpectedfields'); 
    }

    next('route');
  }
}