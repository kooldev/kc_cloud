import { version } from '../../package.json';
import { Router } from 'express';
import formidable from 'express-formidable';
import sys from 'sys';
import fs from 'fs-extra';
import url from 'url';
import xmltojson from 'xml2json';
import { spawn } from 'child_process';
import path from 'path';

const intToDigitString = (index, len) => {
	const strInt = (index + 1).toString();
	return new Array(len - strInt.length + 1).join('0').concat(strInt)
};

const takename = (path) => {
	const pathArray = path.split('/');
	return pathArray.reduce((r, a, i) => {
			if (i == 0) { return a; }
			else if (i == 2) { return r.concat('_').concat(a); }
			else { return r; } }
	, '');
}

const shotPath = function (user, path) {
	return `${__dirname}/../../files/${user}/${path}`
};

const exportFolder = function (folderName) {
	const exportFolder = `${__dirname}/../../exports/${folderName}/`;
	fs.ensureDirSync(exportFolder);	
	return exportFolder;
}

const createThumbnail = (user, path, config, id) => {
	const imagemagik = spawn(`${config.convert}`, 
		[
			`${ shotPath(user, path) }/png/view${id}.png`,
			`${exportFolder('thumbnails')}${takename(path)}.jpg`
		]);	
		imagemagik.stderr.on('data', (data) => { 
			/*TODO treat error*/ 
			console.log("error createThumbnail",`${data}`);
		});
}

export default ({ config, db }) => {
	const api = Router();
	
	api.get('/', (req, res) => {
		res.redirect('/takes');
	});

	api.get('/takes', (req, res) => {	
		fs.readdir(exportFolder('shots'), function(err, filenames) {
			let filmsA = filenames.filter(path => { 
				return (path != '.DS_Store' && path != '.' && path != '..')  
			}).map(filename => path.parse(filename).name);
		
			res.render('takes', { filmArray: filmsA });
		});
		
	});

	api.get('/encode', (req, res) => {
		const query = url.parse(req.url, true, true).query;
		const fps = query.fps || '12.5';
		const shotFolderpath = shotPath(query.user, query.path);
		var firstid="";
		const shotxml = `${shotFolderpath}/shot.xml`;
		fs.readFile(shotxml, 'utf8', (err, data) => {
			if (err) throw err;
			try {
				const symlinksFolder = `${ shotFolderpath }/symlinks`;
				fs.emptyDir(symlinksFolder, err => {
				  	if (err) return console.error(err)
				  		
					fs.ensureDirSync(symlinksFolder);
					const json =  JSON.parse(xmltojson.toJson(data));
					json.shot.timeline.layers.layer.image.forEach((image, index) => {
						if(index==0){
							firstid = image.id;
						}
						const symimagepath =`${ symlinksFolder }/view${ intToDigitString(index, 7) }.png`
						const realimagepath =`${ shotFolderpath }/png/view${image.id}.png`
						const child  = spawn('/bin/ln', ['-s', realimagepath, symimagepath]);
						
						child.stderr.on('data', (data) => {
							/*TODO treat error*/
							//console.log("error symlink",`${data}`);
						});
					});

					const symlinksSequence =`${ shotFolderpath }/symlinks/view%07d.png`;
					const filmOutput =  `${exportFolder('shots')}${takename(query.path)}.mp4`;
					const ffmpeg = spawn(`${config.ffmpeg}`, 
					['-y', '-framerate', `${fps}`, '-i', `${symlinksSequence}`, 
					'-c:v', 'libx264', 
					'-pix_fmt', 'yuv420p', `${ filmOutput }`
					]);	
					ffmpeg.stderr.on('data', (data) => { 
						/*TODO treat error*/ 
						//console.log("error ffmpeg",`${data}`);
					})
					createThumbnail(query.user, query.path, config, firstid);	
				})
				
			} catch (e) {
				res.send({ session: false, error:e });		
				return
			}
			res.send({ffmpeg:`${config.ffmpeg}`,convert:`${config.ffmpeg}`});
		});
	});

	api.post('/upload', (req, res) => {
		Object.keys(req.files).forEach(function(key) {
			const folderName = `${__dirname}/../../files/${req.fields.user}/${req.fields.path}`
			fs.ensureDirSync(folderName);
			fs.rename(req.files[key].path, `${folderName}/${req.files[key].name}`);
		});
		res.send({ session: true, success: true });
	});

	return api;
}
