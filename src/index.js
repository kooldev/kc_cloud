import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import api from './api';
import config from './config.json';
import formidable from 'express-formidable';
import ejs from 'ejs';
import EventEmitter from 'events';
import errors from './middleware/error';
import path from 'path';

const app = express();
app.use(express.static(path.join(__dirname, '/../public')));
app.server = http.createServer(app);
app.use(morgan('dev'));
app.use(formidable({'maxFileSize':10 * 1024 * 1024}));
app.set('view engine', 'ejs');
app.set('views', './views')
app.use(cors({
	exposedHeaders: config.corsHeaders
}));

app.use(bodyParser.json({limit: '150mb'}));
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
limit: '150mb',
extended: true
}));
app.use(bodyParser.json({
	limit : config.bodyLimit
}));

initializeDb( db => {
	app.use(middleware({ config, db }));
	app.use(errors());
	app.use('/', api({ config, db }));
	
	const emitter = new EventEmitter()
	emitter.setMaxListeners(0)
	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;
