import loki from 'lokijs'
import bcrypt from 'bcryptjs';

export default callback => {
	function databaseInitialize() {
		
		if (db.getCollection("users")) {
			db.getCollection("users").clear();
		}
		
		const users = db.addCollection("users");
	
		users.insert({ user: 'johnb', password: bcrypt.hashSync('loving', 8), machines: [{ name: 'Renne1' }] });
		users.insert({ user: 'eric', password: bcrypt.hashSync('arrolla', 8), machines: [{ name: 'Renne1'}] });
		users.insert({ user: 'afca', password: bcrypt.hashSync('LamenagerieRocks', 8), machines: [{ name: 'Renne1' }] });
		users.insert({ user: 'lamenagerie', password: bcrypt.hashSync('JohnRocks1', 8), machines: [{ name: 'Renne1' }] });
		users.insert({ user: 'TLS_01', password: bcrypt.hashSync('tls_01', 8), machines: [{ name: 'Renne1' }] });
		users.insert({ user: 'TLS_02', password: bcrypt.hashSync('tls_02', 8), machines: [{ name: 'Renne1' }] });
		users.insert({ user: 'REN_01', password: bcrypt.hashSync('ren_01', 8), machines: [{ name: 'Renne1' }] });
		users.insert({ user: 'REN_02', password: bcrypt.hashSync('ren_02', 8), machines: [{ name: 'Renne1' }] });
		users.insert({ user: 'REN_03', password: bcrypt.hashSync('ren_03', 8), machines: [{ name: 'Renne1' }] });
		users.insert({ user: 'REN_04', password: bcrypt.hashSync('ren_04', 8), machines: [{ name: 'Renne1' }] });
		
		db.saveDatabase();
	}
	// connect to a database if needed, then pass it to `callback`:
	const db = new loki('users.db',
	{
		autoload: true,
		autoloadCallback : databaseInitialize,
		autosave: true, 
		autosaveInterval: 4000
	});
	callback(db);
}
